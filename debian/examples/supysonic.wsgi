# This file is part of Supysonic.
# Supysonic is a Python implementation of the Subsonic server API.
#
# Copyright (C) 2013 Alban 'spl0k' Féron
# Copyright (C) 2021 Louis-Philippe Véronneau <pollo@debian.org>
#
# Distributed under terms of the GNU AGPLv3 license.

import sys, os

path_to_app = "/usr/share/supysonic"

sys.path.insert(0, path_to_app)

from supysonic.web import create_application
application = create_application()
